#!/usr/bin/sh

ln -sf ~/dotfiles/_vimrc ~/_vimrc
ln -sf ~/dotfiles/asciidoc_templete.asciidoc ~/.asciidoc_templete
ln -sf ~/dotfiles/.zshrc ~/.zshrc

